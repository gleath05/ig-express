import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [

  // BACK TO HOME IF PATH EMPTY AFTER LOCALHOST:4200
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
  },

  {
    path: 'home',
    loadChildren: './features/pos-app/pos-app.module#PosAppModule'
  },

  // TYPING USER AFTER LOCALHOST:4200 WILL DIRECT YOU TO ADMIN
  {
    path: 'user',
    redirectTo: 'user/admin/home-admin',
    pathMatch: 'full'
  },

  {
    path: 'user',
    loadChildren: './features/pos-user/pos-user.module#PosUserModule',
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
