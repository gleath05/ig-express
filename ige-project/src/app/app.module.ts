import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AngularFireModule } from '@angular/fire';
import { environment } from 'src/environments/environment';
import { PosAppModule } from './features/pos-app/pos-app.module';
import { ReactiveFormsModule } from '@angular/forms';
import { NavBarComponent } from './features/pos-app/components/nav-bar/nav-bar.component';
import { PosUserModule } from './features/pos-user/pos-user.module';
import { AuthenticationService } from './features/pos-user/shared/authentication.services';
import { AuthenticationGuard } from './features/pos-user/shared/authenticationGuard.service';
import { ToastrModule } from 'ngx-toastr';

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    PosAppModule,
    ReactiveFormsModule,
    PosUserModule,
    CommonModule,
    ToastrModule.forRoot()

  ],
  providers: [
    AuthenticationService,
    AuthenticationGuard,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
