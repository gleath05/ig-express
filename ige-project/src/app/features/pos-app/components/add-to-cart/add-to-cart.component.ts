import { Component, OnInit, Input } from '@angular/core';
import { MyProducts } from '../../../shared/models/myProducts.model';
import { AddingProductsService } from '../../../shared/services/adding-products.service';

@Component({
  selector: 'app-add-to-cart',
  templateUrl: './add-to-cart.component.html',
  styleUrls: ['./add-to-cart.component.scss']
})
export class AddToCartComponent implements OnInit {
  cartProducts: MyProducts[];

  showDataNotFound = true;

	// Not Found Message
	messageTitle = 'No Products Found in Cart';
	messageDescription = 'Please, Add Products to Cart';

  constructor(private service: AddingProductsService) { }

  ngOnInit() {
    this.getCartProduct();
  }

  removeCartProduct(product: MyProducts) {
		this.service.removeLocalCartProduct(product);

		// Recalling
		this.getCartProduct();
	}

  getCartProduct() {
		this.cartProducts = this.service.getLocalCartProducts();
	}

}
