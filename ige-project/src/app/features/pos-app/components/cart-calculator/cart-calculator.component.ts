import { Component, OnInit, Input, SimpleChanges, SimpleChange } from '@angular/core';
import { MyProducts } from '../../../shared/models/myProducts.model';

@Component({
  selector: 'app-cart-calculator',
  templateUrl: './cart-calculator.component.html',
  styleUrls: ['./cart-calculator.component.scss']
})
export class CartCalculatorComponent implements OnInit {
  @Input() products: MyProducts[];

  totalValue = 0;

  constructor() { }

  ngOnChanges(changes: SimpleChanges) {
		const dataChanges: SimpleChange = changes.products;

		const products: MyProducts[] = dataChanges.currentValue;
		this.totalValue = 0;
		products.forEach((product) => {
			this.totalValue += product.price;
		});
	}

  ngOnInit() {
  }

}
