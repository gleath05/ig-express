import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-csr-request',
  templateUrl: './csr-request.component.html',
  styleUrls: ['./csr-request.component.scss']
})
export class CsrRequestComponent implements OnInit {

  @Output() csrFormValueOut = new EventEmitter<any>();

  constructor(private fb: FormBuilder) { }

    csrServiceForm: FormGroup;

  ngOnInit() {
    this.csrServiceForm = this.fb.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.pattern('^[^\\s@]+@[^\\s@]+\\.[^\\s@]{3,}$')]],
      topic: ['', Validators.required],
      type: ['', Validators.required],
      title: ['', Validators.required],
      message: ['', Validators.required]
    });
  }

  onCustomerSubmit(csrFormValue) {
    if (this.csrServiceForm.valid) {
      this.csrFormValueOut.emit(csrFormValue);
      console.log('csr-ts1', csrFormValue);
      console.log('csr-ts2', this.csrServiceForm.value);
      this.csrServiceForm.reset();
    }
  }

}
