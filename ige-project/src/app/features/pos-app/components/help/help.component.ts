import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-help',
  templateUrl: './help.component.html',
  styleUrls: ['./help.component.scss']
})
export class HelpComponent {

  @Output() targetCsrOut = new EventEmitter<any>();
  @Output() targetTsrOut = new EventEmitter<any>();

  customerService() {
    this.targetCsrOut.emit(true);
    console.log('csr', this.targetCsrOut);
  }

  technicalSupport() {
    this.targetTsrOut.emit(true);
    console.log('tsr', this.targetTsrOut);
  }

}
