import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AddingProductsService } from '../../../shared/services/adding-products.service';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {

  constructor(private router: Router,
              private service: AddingProductsService) { }

  ngOnInit() {
  }

/*   routeAsus(pass) {
    this.router.navigate(['/shopping-cart'], {fragment: pass});
  } */

  goToCart() {
    this.router.navigateByUrl('my-cart');
  }

  goToHelp() {
    this.redirectTo('help');
  }

  redirectTo(uri: string) {
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(() =>
    this.router.navigate([uri]));
  }

}
