import { Component, OnInit } from '@angular/core';
import { AddingProductsService } from 'src/app/features/shared/services/adding-products.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute } from '@angular/router';
import { MyProducts } from 'src/app/features/shared/models/myProducts.model';

@Component({
  selector: 'app-product-info',
  templateUrl: './product-info.component.html',
  styleUrls: ['./product-info.component.scss']
})
export class ProductInfoComponent implements OnInit {

  private sub: any;
  product: MyProducts;

  constructor(
    private route: ActivatedRoute,
    private service: AddingProductsService,
    private toastrService: ToastrService,
  ) {
    this.product = new MyProducts();
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe((params) => {
      const id = params.id; // (+) converts string 'id' to a number
      this.getProductInfo(id);
    });
  }

  getProductInfo(id: string) {
    // this.spinnerService.show();
    const x = this.service.getProductById(id);
    x.snapshotChanges().subscribe(
      (product) => {
        // this.spinnerService.hide();
        const y = product.payload.toJSON() as MyProducts;

        y.$key = id;
        this.product = y;
      },
      (error) => {
        this.toastrService.error('Error while fetching Product Detail', error);
      }
    );

  }

  addToCart(product: MyProducts) {
		this.service.addToCart(product);
	}

	ngOnDestroy() {
		this.sub.unsubscribe();
	}
}
