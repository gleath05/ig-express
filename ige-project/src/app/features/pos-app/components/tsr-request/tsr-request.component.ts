import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-tsr-request',
  templateUrl: './tsr-request.component.html',
  styleUrls: ['./tsr-request.component.scss']
})
export class TsrRequestComponent implements OnInit {

  @Output() tsrFormValueOut = new EventEmitter<any>();

  tsrRequestForm: FormGroup;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.tsrRequestForm = this.fb.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.pattern('^[^\\s@]+@[^\\s@]+\\.[^\\s@]{3,}$')]],
      topic: ['', Validators.required],
      type: ['', Validators.required],
      title: ['', Validators.required],
      message: ['', Validators.required]
    });
  }

  onCustomerSubmit(tsrFormValue) {
    if (this.tsrRequestForm.valid) {
      console.log('tsr-ts1', tsrFormValue);
      this.tsrFormValueOut.emit(tsrFormValue);
      console.log('tsr-ts2', this.tsrRequestForm.value);
      this.tsrRequestForm.reset();
    }
  }

}
