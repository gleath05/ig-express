import { Component } from '@angular/core';
import { CustomerService } from 'src/app/features/shared/services/customer.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-main-help',
  templateUrl: './main-help.component.html',
  styleUrls: ['./main-help.component.scss']
})
export class MainHelpComponent {

  public targetHelp = true;
  public targetTsr = false;
  public targetCsr = false;

  constructor(
    private customerService: CustomerService,
    private toastr: ToastrService) { }


  tsrFormValueIn(tsrFormValue) {
    console.log('main first', tsrFormValue);
    this.customerService.insertTsrDetails(tsrFormValue);
    console.log('main second', tsrFormValue);
    this.toastr.success('You have been successfully send your request!');
  }

  csrFormValueIn(csrFormValue) {
    console.log('main first', csrFormValue);
    this.customerService.insertCsrDetails(csrFormValue);
    console.log('main second', csrFormValue);
    this.toastr.success('You have been successfully send your request!');
  }

  tsrTargetIn(tsr) {
    this.targetHelp = false;
    this.targetTsr = tsr;
    this.targetCsr = false;
  }

  csrTargetIn(csr) {
    this.targetHelp = false;
    this.targetTsr = false;
    this.targetCsr = csr;
  }

}
