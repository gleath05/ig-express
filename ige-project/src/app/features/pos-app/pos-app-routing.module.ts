import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PromoComponent } from './components/promo/promo.component';
import { AddToCartComponent } from './components/add-to-cart/add-to-cart.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { ShoppingCartComponent } from './container/shopping-cart/shopping-cart.component';
import { MainHomeComponent } from './container/main-home/main-home.component';
import { MainAboutUsComponent } from './container/main-about-us/main-about-us.component';
import { MainHelpComponent } from './container/main-help/main-help.component';
import { DummyComponent } from './components/dummy/dummy.component';
import { ProductInfoComponent } from './components/product-info/product-info.component';

const routes: Routes = [
  { path: '', component: MainHomeComponent },
  { path: 'about-us', component: MainAboutUsComponent },
  { path: 'promo', component: PromoComponent },
  { path: 'page-not-found', component: PageNotFoundComponent },
  { path: 'my-cart', component: AddToCartComponent },
  { path: 'shopping-cart', component: ShoppingCartComponent },
  { path: 'help', component: MainHelpComponent },
  { path: 'dummy', component: DummyComponent },
  { path: 'product-info/:id', component: ProductInfoComponent },
  { path: '**', redirectTo: 'page-not-found', pathMatch: 'full' }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class PosAppRoutingModule { }
