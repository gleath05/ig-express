import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PosAppRoutingModule } from './pos-app-routing.module';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { HomeComponent } from './components/home/home.component';
import { PromoComponent } from './components/promo/promo.component';
import { AboutUsComponent } from './components/about-us/about-us.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AddToCartComponent } from './components/add-to-cart/add-to-cart.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { CsrRequestComponent } from './components/csr-request/csr-request.component';
import { TsrRequestComponent } from './components/tsr-request/tsr-request.component';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { ShoppingCartComponent } from './container/shopping-cart/shopping-cart.component';
import { PosUserModule } from '../pos-user/pos-user.module';
import { MainHomeComponent } from './container/main-home/main-home.component';
import { MainHelpComponent } from './container/main-help/main-help.component';
import { MainAboutUsComponent } from './container/main-about-us/main-about-us.component';
import { HelpComponent } from './components/help/help.component';
import { DummyComponent } from './components/dummy/dummy.component';
import { NoProductsFoundComponent } from './components/no-products-found/no-products-found.component';
import { CartCalculatorComponent } from './components/cart-calculator/cart-calculator.component';
import { ProductInfoComponent } from './components/product-info/product-info.component';
import { NgxPaginationModule } from 'ngx-pagination';


@NgModule({
  declarations: [
    DashboardComponent,
    HomeComponent,
    PromoComponent,
    AboutUsComponent,
    AddToCartComponent,
    CsrRequestComponent,
    TsrRequestComponent,
    PageNotFoundComponent,
    ShoppingCartComponent,
    MainHomeComponent,
    MainHelpComponent,
    MainAboutUsComponent,
    HelpComponent,
    DummyComponent,
    NoProductsFoundComponent,
    CartCalculatorComponent,
    ProductInfoComponent,

  ],
  imports: [
    CommonModule,
    PosAppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    AngularFireStorageModule,
    AngularFireDatabaseModule,
    MDBBootstrapModule.forRoot(),
    PosUserModule,
    NgxPaginationModule
  ]
})

export class PosAppModule { }
