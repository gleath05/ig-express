import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.scss']
})
export class AddProductComponent implements OnInit {

  @Output() formValueOut = new EventEmitter<string>();

  imgSrc: string;
  selectedImage: any = null;
  isSubmitted: boolean;
  formTemplate: FormGroup;

  constructor(
    private fb: FormBuilder,
    private toastr: ToastrService) { }

  ngOnInit() {
    this.formTemplate = this.fb.group({
      prodName: [null, Validators.required],
      category: [null],
      price: [null, Validators.required],
      qty: [null, Validators.required],
      imageUrl: [null, Validators.required],
      selectImage: ['']
    });
    this.resetForm();
  }

  showPreview(event: any) {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.onload = (e: any) => this.imgSrc = e.target.result;
      reader.readAsDataURL(event.target.files[0]);
      this.formTemplate.get('selectImage').patchValue(event.target.files[0]);
    } else {
      this.imgSrc = '/src/assets/img/image_placeholder.jpg';
      this.selectedImage = null;
    }
  }

  onSubmit(formValue) {
    this.isSubmitted = true;
    if (this.formTemplate.valid) {
      this.formValueOut.emit(formValue);
      console.log(formValue);
      this.resetForm();
      this.toastr.success('Adding Success!');
    } else {
      this.toastr.error('Adding Unsuccessful, please fill up everything!');
    }
  }

  get formControls() {
    return this.formTemplate.controls;
  }

  resetForm() {
    this.formTemplate.reset();
    this.formTemplate.setValue({
      prodName: '',
      price: '',
      qty: '',
      imageUrl: '',
      category: '',
      selectImage: ''
    });
    this.imgSrc = '/assets/img/image_placeholder.jpg';
    this.selectedImage = null;
    this.isSubmitted = false;
  }
}
