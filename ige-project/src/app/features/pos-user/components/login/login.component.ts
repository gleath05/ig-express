import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../shared/authentication.services';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {

  errorMsg: string;

  constructor(
    private formbuilder: FormBuilder,
    private authService: AuthenticationService,
    private router: Router,
    private toastr: ToastrService) { }

  loginForm: FormGroup;
  submitted = false;

  ngOnInit() {
    this.loginForm = this.formbuilder.group({
      email: ['', Validators.required],
      password: ['']
    });
  }

  signIn() {

    this.authService.login({
      email: this.loginForm.controls.email.value,
      password: this.loginForm.controls.password.value
    })
      .then(res => {
        this.toastr.success('Login Success!');
        this.router.navigate(['/user/admin/home-admin']);
      }, err => {
        console.log(err);
        // alert(err);
        alert('Invalid Email or Passowrd');
        this.toastr.error(err);

      });
  }
}
