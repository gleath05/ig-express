import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AddingProductsService } from 'src/app/features/shared/services/adding-products.service';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss']
})
export class ProductDetailsComponent implements OnInit {
  showSuccessMessage: boolean;
  selectedImage: any = null;
  isSubmitted: boolean;
  formTemplate: FormGroup;

  imageArray: any[];
  showDeletedMessage: boolean;
  searchText: any = '';

  addingCart: any[];

  constructor(
    private service: AddingProductsService,
    private fb: FormBuilder) { }

  // DELETE FUNCTION

  onDelete($key, downloadUrl) {
    if (confirm('Are you sure to delete this record ?')) {
      this.service.deleteDataImage($key);
      this.service.deleteImage(downloadUrl);
      this.showDeletedMessage = true;
      setTimeout(() => this.showDeletedMessage = false, 3000);
    }
  }

  ngOnInit() {
    this.formTemplate = this.fb.group({
      $key: [null],
      prodName: ['', Validators.required],
      category: [''],
      price: ['', Validators.required],
      qty: ['', Validators.required],
      imageUrl: ['', Validators.required],
    });
    this.service.getImageDetailList().subscribe(
      list => {
        this.imageArray = list.map(item => {
          return {
            $key: item.key,
            ...item.payload.val()
          };
        });
      });
  }

  // ADD TO CART FUNCTION

  addCart() {
    this.addingCart = this.imageArray;
  }

  get formControls() {
    return this.formTemplate.controls;
  }

  populateForm(populate) {
    this.formTemplate.setValue(populate);
  }

  filterCondition(image) {
    return image.prodName.toLowerCase().indexOf(this.searchText.toLowerCase()) !== -1;

  }

  // EDIT FUNCTION
  // EDIT WILL NOT WORK WITHOUT THIS
  onImageUpdate(image) {
    this.service.imageDetailList.update(image.$key,
      {
        prodName: image.prodName,
        category: image.category,
        price: image.price,
        qty: image.qty,
      });
  }

  onSave() {
    this.isSubmitted = true;
    if (confirm('Are you sure to save this record ?')) {
      this.onImageUpdate(this.formTemplate.value);
      this.showSuccessMessage = true;
      setTimeout(() => this.showSuccessMessage = false, 3000);
      this.isSubmitted = false;

      this.formTemplate.reset();

      this.formTemplate.setValue({
        $key: null,
        prodName: '',
        price: '',
        qty: '',
        category: '',
        imageUrl: ''
      });
    }
  }

}
