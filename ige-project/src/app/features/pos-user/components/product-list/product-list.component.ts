import { Component, OnInit } from '@angular/core';
import { AddingProductsService } from '../../../shared/services/adding-products.service';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { MyProducts } from '../../../shared/models/myProducts.model';


@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {

  productList: MyProducts[];
  imageArray: any[];
  rowIndexArray: any[];
  formTemplate: FormGroup;

  searchAsus = 'asus';
  searchSamsung = 'samsung';
  searchHuawei = 'huawei';
  searchOppo = 'oppo';
  searchIphone = 'iphone';
  toastrService: any;

  constructor(
    private service: AddingProductsService,
    private fb: FormBuilder,
    private router: Router) {
  }

  ngOnInit() {

    const x = this.service.getImageDetailList();
    x.subscribe(
      (product) => {
        this.productList = [];
        product.forEach((element) => {
          const y = element.payload.toJSON();
          y['$key'] = element.key;
          this.productList.push(y as MyProducts);

        });
      },
      (err) => {
        this.toastrService.error('Error while fetching Products', err);
      }
    );



    this.formTemplate = this.fb.group({
      $key: [null],
      prodName: ['', Validators.required],
      category: [''],
      price: ['', Validators.required],
      qty: ['', Validators.required],
      imageUrl: ['', Validators.required],
    });
    this.service.getImageDetailList().subscribe(
      list => {
        this.imageArray = list.map(item => {
          return {
            $key: item.key,
            ...item.payload.val()
          };

        });
      });
  }


  addToCart(product: MyProducts) {
    this.service.addToCart(product);
    console.log(product);
  }



  // Search per product

  filterAsus(product) {
    return product.category.toLowerCase().indexOf(this.searchAsus.toLowerCase()) !== -1;
  }

  filterSamsung(product) {
    return product.category.toLowerCase().indexOf(this.searchSamsung.toLowerCase()) !== -1;
  }
  filterHuawei(product) {
    return product.category.toLowerCase().indexOf(this.searchHuawei.toLowerCase()) !== -1;
  }
  filterOppo(product) {
    return product.category.toLowerCase().indexOf(this.searchOppo.toLowerCase()) !== -1;
  }
  filterIphone(product) {
    return product.category.toLowerCase().indexOf(this.searchIphone.toLowerCase()) !== -1;
  }




}
