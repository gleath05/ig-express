import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import { Register } from '../../shared/register.model';
import { PasswordValidator } from '../../shared/password.validator';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  @Output() regFormValueOut = new EventEmitter<any>();

  user: Register = new Register();
  registrationForm: FormGroup;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.registrationForm = this.fb.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.pattern('^[^\\s@]+@[^\\s@]+\\.[^\\s@]{3,}$')]],
      role: ['', Validators.required],
      password: ['', [Validators.required]],
      verifyPassword: ['', [Validators.required]]
    }, {
      validator: PasswordValidator
    });
  }

  onRegistrationSubmit(regFormValue) {
    if (this.registrationForm.valid) {
      this.regFormValueOut.emit(regFormValue);
      console.log('reg', this.registrationForm.value);
      this.registrationForm.reset();
      this.registrationForm.markAsPristine();
      this.registrationForm.markAsUntouched();
    }
  }
}
