import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../shared/authentication.services';
import { ToastrService } from 'ngx-toastr';
import { AddUserService } from '../../shared/add-user.service';

@Component({
  selector: 'app-main-register',
  templateUrl: './main-register.component.html',
  styleUrls: ['./main-register.component.scss']
})
export class MainRegisterComponent implements OnInit {

  constructor(
    private authService: AuthenticationService,
    private toastr: ToastrService,
    private adduser: AddUserService) { }

  ngOnInit() {
  }

  regFormValueIn(regFormValue) {
    console.log('user', regFormValue);
    this.authService.register({
      email: regFormValue.email,
      password: regFormValue.password
    })
      .then((result) => {
        console.log('second', regFormValue);
        this.adduser.insertUserDetails(regFormValue);
        this.toastr.success('You have been successfully registered!');
        console.log(result.user);
      }).catch((error) => {
        this.toastr.success(error.message);
      });
  }
}
