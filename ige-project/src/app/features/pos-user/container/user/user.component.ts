import { Component, OnInit } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { finalize } from 'rxjs/operators';
import { AddingProductsService } from 'src/app/features/shared/services/adding-products.service';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../shared/authentication.services';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  imgSrc: string;
  selectedImage: any = null;
  isSubmitted: boolean;

  constructor(
    private storage: AngularFireStorage,
    private service: AddingProductsService,
    private router: Router,
    private authService: AuthenticationService,
    private toastr: ToastrService, ) { }

  ngOnInit() {
  }

  logOut() {
    this.authService.logout();
    alert('are you sure you want to logout??');
    this.router.navigateByUrl('/user/login');
    this.toastr.warning('Logout Success!');
  }

  navigateHome() {
    this.router.navigateByUrl('user/admin/home-admin');
  }

  navigateInventory() {
    this.router.navigateByUrl('user/admin/product-details');
  }

  navigateMyProducts() {
    this.router.navigateByUrl('user/admin/product-list');
  }

  navigateCreateUser() {
    this.router.navigateByUrl('user/admin/register-employee');
  }

  formValueIn(formValue) {
    console.log(formValue);
    const filePath = `${formValue.category}/${formValue.selectImage.name.split('.').slice(0, -1).join('.')}_${new Date().getTime()}`;
    const fileRef = this.storage.ref(filePath);
    this.storage.upload(filePath, formValue.selectImage).snapshotChanges().pipe(
      finalize(() => {
        fileRef.getDownloadURL().subscribe((url) => {
          formValue.imageUrl = url;
          console.log(formValue);
          this.service.insertImageDetails(formValue);

        });
      })
    ).subscribe();
  }
}
