import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserComponent } from './container/user/user.component';
import { ProductListComponent } from './components/product-list/product-list.component';
import { ProductDetailsComponent } from './components/product-details/product-details.component';
import { AddProductComponent } from './components/add-product/add-product.component';
import { LoginComponent } from './components/login/login.component';
import { AuthenticationGuard } from './shared/authenticationGuard.service';
import { HomeAdminComponent } from './components/home-admin/home-admin.component';
import { MainRegisterComponent } from './container/main-register/main-register.component';

const routes: Routes = [
  {
    path: 'admin', component: UserComponent, canActivate: [AuthenticationGuard],
    children: [
      /* { path: 'admin', component: UserComponent }, */
      { path: 'product-list', component: ProductListComponent, canActivate: [AuthenticationGuard] },
      { path: 'product-details', component: ProductDetailsComponent, canActivate: [AuthenticationGuard] },
      { path: 'add-product', component: AddProductComponent, canActivate: [AuthenticationGuard] },
      { path: 'register-employee', component: MainRegisterComponent, canActivate: [AuthenticationGuard] },
      { path: 'home-admin', component: HomeAdminComponent, canActivate: [AuthenticationGuard] },
    ]
  },
  { path: 'login', component: LoginComponent },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PosUserRoutingModule { }
