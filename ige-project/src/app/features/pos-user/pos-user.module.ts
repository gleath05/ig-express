import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PosUserRoutingModule } from './pos-user-routing.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { ProductDetailsComponent } from './components/product-details/product-details.component';
import { AddProductComponent } from './components/add-product/add-product.component';
import { UserComponent } from './container/user/user.component';
import { ProductListComponent } from './components/product-list/product-list.component';
import { LoginComponent } from './components/login/login.component';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { RegisterComponent } from './components/register/register.component';
import { HomeAdminComponent } from './components/home-admin/home-admin.component';
import { MainRegisterComponent } from './container/main-register/main-register.component';
import { NgxPaginationModule } from 'ngx-pagination';


@NgModule({
  declarations: [
    ProductDetailsComponent,
    ProductListComponent,
    AddProductComponent,
    UserComponent,
    LoginComponent,
    RegisterComponent,
    HomeAdminComponent,
    MainRegisterComponent,
  ],
  imports: [
    CommonModule,
    PosUserRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    AngularFireStorageModule,
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    NgxPaginationModule
  ],

  exports: [
    ProductListComponent
  ]

})
export class PosUserModule { }
