import { Injectable } from '@angular/core';
import { AngularFireList, AngularFireDatabase } from '@angular/fire/database';
import { AngularFireStorage } from '@angular/fire/storage';

@Injectable({
    providedIn: 'root'
})
export class AddUserService {

    userDetailList: AngularFireList<any>;

    constructor(
        private firebase: AngularFireDatabase,
        private fireStorage: AngularFireStorage) {
        this.userDetailList = firebase.list('usersList');
    }

    insertUserDetails(userList) {
        this.userDetailList.push(userList);
    }
}
