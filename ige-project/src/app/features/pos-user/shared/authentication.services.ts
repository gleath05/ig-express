import { Observable } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
import { Injectable } from '@angular/core';
import * as firebase from 'firebase/app';
import { User } from './user.model';
import { Register } from './register.model';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private user: Observable<firebase.User>;
  private reg: Observable<Register>;

  constructor(private afAuth: AngularFireAuth) {
    this.user = afAuth.authState;
    this.reg = afAuth.authState;
  }

  login(user: User) {
    return this.afAuth.auth.signInWithEmailAndPassword(user.email, user.password);
  }

  register(reg: Register) {
    return this.afAuth.auth.createUserWithEmailAndPassword(reg.email, reg.password);
  }

  logout() {
    return this.afAuth.auth.signOut();
  }

  authUser() {
    return this.user;
  }
}
