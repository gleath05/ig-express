import { AbstractControl } from '@angular/forms';

export function PasswordValidator(control: AbstractControl): { [key: string]: boolean } | null {
    const password = control.get('password');
    const verifyPassword = control.get('verifyPassword');
    if (password.pristine || verifyPassword.pristine) {
        return null;
    }
    return password && verifyPassword && password.value !== verifyPassword.value ?
        { passwordsDoNotMatch: true } : null;
}

export function ValidateUrl(control: AbstractControl) {
    if (!control.value.startsWith('https') || !control.value.includes('.io')) {
      return { validUrl: true };
    }
    return null;
  }
