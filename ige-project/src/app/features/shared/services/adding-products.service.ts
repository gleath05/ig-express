import { Injectable } from '@angular/core';
import { AngularFireList, AngularFireDatabase, AngularFireObject } from '@angular/fire/database';
import { AngularFireStorage } from '@angular/fire/storage';
import { MyProducts } from '../models/myProducts.model';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class AddingProductsService {

  navbarCartCount = 0;

  imageDetailList: AngularFireList<any>;
  product: AngularFireObject<MyProducts>;

  constructor(
    private firebase: AngularFireDatabase,
    private fireStorage: AngularFireStorage,
    private toastrService: ToastrService
  ) {

    this.calculateLocalCartProdCounts();
    this.imageDetailList = this.firebase.list('imageDetails');
  }

  /*
    ----------  Cart Product Function  ----------
   */

  // Adding new Product to cart db if logged in else localStorage
  addToCart(data: MyProducts): void {
    let a: MyProducts[];

    a = JSON.parse(localStorage.getItem('avct_item')) || [];

    a.push(data);

    localStorage.setItem('avct_item', JSON.stringify(a));
    this.calculateLocalCartProdCounts();
    /* 	this.toastrService.wait('Adding Product to Cart', 'Product Adding to the cart');
      setTimeout(() => {

      }, 100); */
  }

  // Removing cart from local
  removeLocalCartProduct(product: MyProducts) {
    const products: MyProducts[] = JSON.parse(localStorage.getItem('avct_item'));

    for (let i = 0; i < products.length; i++) {
      if (products[i].prodId === product.prodId) {
        products.splice(i, 1);
        break;
      }
    }

    // ReAdding the products after remove
    localStorage.setItem('avct_item', JSON.stringify(products));
    this.calculateLocalCartProdCounts();
  }

  // Fetching Locat CartsProducts
  getLocalCartProducts(): MyProducts[] {
    const products: MyProducts[] = JSON.parse(localStorage.getItem('avct_item')) || [];

    return products;
  }

  // returning LocalCarts Product Count
  calculateLocalCartProdCounts() {
    this.navbarCartCount = this.getLocalCartProducts().length;
  }

  getProductById(key: string) {
		this.product = this.firebase.object('imageDetails/' + key);
		return this.product;
	}

  getImageDetailList() {
    this.imageDetailList = this.firebase.list('imageDetails');
    return this.imageDetailList.snapshotChanges();
  }

  insertImageDetails(imageDetails) {
    this.imageDetailList.push(imageDetails);
  }

  deleteDataImage($key: string) {
    this.imageDetailList.remove($key);
  }
  deleteImage(downloadUrl: string) {
    return this.fireStorage.storage.refFromURL(downloadUrl).delete();
  }

}
