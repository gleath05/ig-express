import { Injectable } from '@angular/core';
import { AngularFireList, AngularFireDatabase } from '@angular/fire/database';
import { AngularFireStorage } from '@angular/fire/storage';

@Injectable({
    providedIn: 'root'
})
export class CustomerService {

    tsrDetailList: AngularFireList<any>;
    csrDetailList: AngularFireList<any>;

    constructor(
        private firebase: AngularFireDatabase,
        private fireStorage: AngularFireStorage) {
        this.tsrDetailList = firebase.list('tsrList');
        this.csrDetailList = firebase.list('csrList');
    }

    getTsrDetailList() {
        this.tsrDetailList = this.firebase.list('tsrList');
        return this.tsrDetailList.snapshotChanges();
    }


    getCsrDetailList() {
        this.tsrDetailList = this.firebase.list('csrList');
        return this.tsrDetailList.snapshotChanges();
    }


    insertTsrDetails(tsrList) {
        this.tsrDetailList.push(tsrList);
    }

    insertCsrDetails(csrList) {
        this.csrDetailList.push(csrList);
    }

}
